module bitbucket.org/drhttp/moesifapi-go

go 1.12

require (
	bitbucket.org/drhttp/moesifapi-go v1.0.3
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
)
